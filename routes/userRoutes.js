const express = require('express')
const router = express.Router()
const userControllers = require('../controllers/userControllers')
const orderControllers = require('../controllers/orderControllers')
const auth = require('../auth')
const {verify , verifyAdmin} = auth

const {
	userRegister,
	userLogin,
	userSetAdmin,
	userDetails,
	allUsers
} = userControllers


router.post('/register', userRegister);

router.post('/login', userLogin);

router.put('/setAsAdmin/:id', verify, verifyAdmin, userSetAdmin);

router.get('/details', verify, userDetails);

router.get('/allUser', verify, verifyAdmin, allUsers)


module.exports = router

