const Order = require('../models/Order')
const User = require('../models/User')
const Product = require('../models/Product')

module.exports.createOrder = async (req,res) => {
	try {
		const product = await Product.findOne({productId: req.body.productId})
		const newOrder = new Order ({
			userId: req.body.userId,
			products: {
				productId: req.body.productId,
				quantity: req.body.quantity
			},
			totalAmount: product.price * req.body.quantity
		})
		await newOrder.save()
		return res.send({newOrder, message: "Order submitted"})
	} catch(error) {
		return res.send(error.message)
	}
}


/*module.exports.createOrder = async (req,res) => {
	try {
		let products = req.body.products
		const productsList = []
		console.log(productsList)
		for (var i = 0; i < products.length; i++) {
			productsList.push({
				product: await Product.findById(products[i].productId),
				quantity: products[i].quantity
			})
		}
		console.log(productsList)
		const newOrder = new Order ({
			userId: req.body.userId,
			products: req.body.products,
			totalAmount: product.price * req.body.quantity
		}) (.reduce here)
		await newOrder.save()
		return res.send({newOrder, message: "Order submitted"})
	} catch(error) {
		return res.send(error.message)
	}
}*/



module.exports.retrieveAllOrder = (req,res) => {
	Order.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}

module.exports.retrieveAuthOrder = (req,res) => {
	Order.find({userId: req.user.id})
	.then(result => res.send(result))
	.catch(error => res.send(error))
}
